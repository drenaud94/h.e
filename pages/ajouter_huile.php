<?php
include '../config/configuration.php';

?>
<div class="cell large-8 add_form">
    <form action="./data/add_huile.php" method="post" enctype="multipart/form-data">
        <div>
            <label for="">Nom</label>
            <input type="text" name="nom">
            <label for="">Description</label>
            <textarea name="desc" id="" cols="30" rows="10"></textarea>
            
        </div>
        <div>
            <input type="file" name="img" >
        </div>
        <div>
            <h3>Problèmes traités</h3>
            <div class="grid-container">
                <div class="grid-x probleme">
                    
                    <?php
                        $data=$bdd->query('SELECT nom FROM probleme');
                        while ($donnee = $data->fetch()) 
                        {
                            ?>
                            <div class=" cell large-4 probleme__bloc">
                                <label for=""><?= $donnee['nom'];?></label>
                                <input type="checkbox" name="probleme[]" id="" class="probleme__input" value="<?= $donnee['nom'];?>">
                            </div>
                            <?php
                        }
                    ?>
                </div>
                <p id="ajout_pb" style="cursor: pointer;">Ajouter probleme</p>
            </div>
        </div>
        <div id="prop">
            <h3>Propriétés</h3>
            <div>
                <label for="">Propriété 1</label>
                <input type="text" name="propriete[]" id="">
            </div>
            <div>
                <label for="">Propriété 2</label>
                <input type="text" name="propriete[]" id="">
            </div>
            <div>
                <label for="">Propriété 3</label>
                <input type="text" name="propriete[]" id="">
            </div>
            <div>
                <label for="">Propriété' 4</label>
                <input type="text" name="propriete[]" id="">
            </div>
        </div>
        <p id="ajout_prop" style="cursor: pointer;">Ajouter propriété</p>
        <div>
            <h3>Mode d'utilisation</h3>
            <div>
                <label for="">Diffusion</label>
                <select name="mode_diffusion" id="" required>
                    <option value="Non">Non</option>
                    <option value="Possible">Possible</option>
                    <option value="Conseillé">Conseillé</option>
                    <option value="priviligier">A priviligier</option>
                </select>
            </div>
            <div>
                <label for="">Usage Alimentaire</label>
                <select name="mode_alimentaire" id="" required>
                    <option value="Non">Non</option>
                    <option value="Possible">Possible</option>
                    <option value="Conseillé">Conseillé</option>
                    <option value="priviligier">A priviligier</option>
                </select>
            </div>
            <div>
                <label for="">Massage</label>
                <select name="mode_massage" id="" required>
                    <option value="Non">Non</option>
                    <option value="Possible">Possible</option>
                    <option value="Conseillé">Conseillé</option>
                    <option value="priviligier">A priviligier</option>
                </select>
            </div>
            <div>
                <label for="">Hygiène et Bain</label>
                <select name="mode_hygiene" id="" required>
                    <option value="Non">Non</option>
                    <option value="Possible">Possible</option>
                    <option value="Conseillé">Conseillé</option>
                    <option value="priviligier">A priviligier</option>
                </select>
            </div>
        </div>
        <div>
            <button type="submit" class="button">Envoyer</button>
        </div>
    </form>
</div>

<script>
    $('#ajout_pb').click(function(){
        $('.probleme').append('<div class=" cell large-4 probleme__bloc"><input type="text" name="new_probleme[]" class="probleme__input" placeholder="Nouveau probleme"></div>')
    })
    $('#ajout_prop').click(function(){
        $('#prop').append('<div><label for="">Nouvelle propriété</label><input type="text" name="propriete[]" id=""></div>')
    })
</script>