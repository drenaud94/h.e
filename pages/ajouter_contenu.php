<?php
include '../config/configuration.php';
?>
<div class="cell large-5" style="padding: 2%; border:1px solid; margin:2%;">

    <form action="./data/add_contenu.php" method="post">
    <div>
    <label for="">Sélectionner le type de contenu à ajouter:</label>
        <select name="id" id="option">
            <option value=""></option>
            <?php
                $data=$bdd->query('SELECT * FROM type_contenu');
                while($types=$data->fetch())
                {
                    ?>
                    <option value="<?=$types['id'];?>"><?=$types['nom'];?></option>
                    <?php
                }
            ?>
        </select>
    </div>
    
    <div id="form_contenu">
    </div >
        
    </form>
</div>
<script>
    $('#option').change(function()
    {
        var type = $('#option option:selected').val()
        console.log(type)
        var test = $.isNumeric(type)
        $('#form_contenu').html('')
        // si la valeur du select est bien un chiffre
        if (test ===true) 
        {
            if (type==1) 
            {
                $('#form_contenu').append('<div><label for="">Nom : </label>  <input type="text" name="nom"></div>  <div><label for="">Description : </label><textarea name="desc" id="" cols="30" rows="4" placeholder="A la fin de chaque étape mettez un +"></textarea></div> <div><button type="submit" class="button">Envoyer l\'exercice</button></div>');
            }
            else if(type==2)
            {
                $('#form_contenu').append('<div><label for="">Nom : </label>  <input type="text" name="nom"></div> <div><label for="">Lien : </label><input type="url" name="url" id=""></div> <div>     <button type="submit" class="button">Envoyer la vidéo</button></div>');
            }
        }
            
    })

</script>