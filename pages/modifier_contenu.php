<?php
include '../config/configuration.php';
?>
<div class="cell large-3" style="padding: 2%; margin:2%;">
        <div>
            <label for="">Sélectionner le type de contenu à modifier:</label>
            <select name="id" id="option">
                <option value=""></option>
                <?php
                    $data=$bdd->query('SELECT * FROM type_contenu');
                    while($types=$data->fetch())
                    {
                        ?>
                        <option value="<?=$types['id'];?>"><?=$types['nom'];?></option>
                        <?php
                    }
                ?>
            </select>
        </div>
</div>   
<div class="cell">
    <div class="edit">
        <div class="grid-container">
            <div class="grid-x grid-margin-x align-center" id="form_contenu">

            </div>
        </div>
    </div>  
</div>
  
        
<script>
    function Delete(id)
    {
        if (confirm("Etes-vous sur de vouloir supprimer ce contenu?"))
        {
            window.location.replace("./data/delete_contenu.php?id="+id+"")
        }
    }
    $('#option').change(function()
    {
        var type = $('#option option:selected').val()
        console.log(type)
        var test = $.isNumeric(type)
        $('#form_contenu').html('')
        // si la valeur du select est bien un chiffre
        if (test ===true) 
        {
            $.ajax({
                url:'./data/info_contenu.php?type='+type,
                dataType:'json',
                success: function(data){
                    
                    var longueur = data.length
                    var l=0;
                    //boucle pour ajouter les résultats obtenus
                    if(type==1)
                    {
                        while (l<longueur) 
                        {
                            $('#form_contenu').append('<div class="cell large-10"><div class="edit__exercice"><form action="./data/update_contenu.php" method="post"><div><label for="">Nom :</label><input type="text" name="nom" value="'+data[l]['nom']+'"><input type="number" name="id" value="'+data[l]['id']+'" style="display:none;"></div>  <div><label for="">Description :</label><textarea name="desc" id="" cols="30" rows="10" placeholder="A la fin de chaque étape mettez un +">'+data[l]['desc']+'</textarea></div> <div><button type="submit" class="button">Modifier l\'exercice</button></div></form> <div><button onclick="Delete('+data[l]['id']+')">Supprimer<button></div></div></div>')
                            var l= l+1
                        }
                    }
                    else if(type==2)
                    {
                        while (l<longueur) 
                        {
                            $('#form_contenu').append('<div class="cell large-4"><div class="edit__video"><form action="./data/update_contenu.php" method="post"><div><label for="">Nom :</label><input type="text" name="nom" value="'+data[l]['nom']+'"><input type="number" name="id" value="'+data[l]['id']+'" style="display:none;"></div>  <div><label for="">Lien :</label><input type="url" name="url" id="" value="'+data[l]['url']+'"></div> <div><button type="submit" class="button">Modifier la vidéo</button></div></form> <div><button onclick="Delete('+data[l]['id']+')">Supprimer<button></div></div></div>')
                            var l= l+1
                        }
                    }
                                        
                },              
            });   
        }
            
    })

</script>