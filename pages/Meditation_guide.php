<?php
include '../config/configuration.php';
$videos=$bdd->query('SELECT * FROM contenu WHERE id_type=2');

while($video=$videos->fetch())
{
    ?>
    <div class="cell medium-6 large-5 meditation_video" style="text-align: center; display:block;">
        <h1><?=$video['nom'];?></h1>
        <iframe width="560" height="315" src="<?=$video['lien'];?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <?php
}
?>
