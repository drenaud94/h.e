<?php
include '../config/configuration.php';
$exercices=$bdd->query('SELECT * FROM contenu WHERE id_type=1');

while ($exercice=$exercices->fetch()) 
{
    $texte=$exercice['description'];
    $texte=explode('+',$texte);
    ?>
        <div class="cell medium-6 large-5 meditation_exo">
            <h2><?=$exercice['nom'];?></h2>
            <ul>
                <?php
                    foreach ($texte as $value) {
                        echo '<li>'.$value.'</li>';
                    }
                ?>       
            </ul>
        </div>
    <?php
}
?>