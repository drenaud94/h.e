<?php
include '../config/configuration.php';

$data=$bdd->query('SELECT * FROM huiles');

while ($donnees = $data->fetch()) 
{
    $id_huile=intval($donnees['id_huile']);
    $nom=$donnees['nom'];
    $desc=$donnees['description'];
    $image=$donnees['image'];

    ?>
         <div class="cell small-4 medium-3 large-3 product-card" onclick="Charge_huile(<?=$id_huile;?>)">
            <div class="product_img">
                <?php
                    if (isset($image)) 
                    {
                        ?>
                            <img src="./ressources/images/huiles/<?=$image;?>" alt="">
                        <?php
                    }
                ?>
            </div>
            <div class="product_desc">
                <h3>Huile essentielle <?=$nom;?></h3>
            </div>
        </div>    
 <?php   
}
?>
<script>
function Charge_huile(id)
{
    $('#contenu').load('./pages/edit_huile.php?id='+id+'')
}
    
</script>