<?php
include '../config/configuration.php';

if (isset($_GET['id'])) 
{
    $id=$_GET['id'];

    $data=$bdd->prepare('SELECT * FROM huiles WHERE id_huile=:id');
    $data->bindParam(':id',$id);
    $data->execute();

    while ($donnees = $data->fetch()) 
    {
        $id_huile=intval($donnees['id_huile']);
        $nom=$donnees['nom'];
        $desc=$donnees['description'];
        $image=$donnees['image'];

        $prop=$bdd->prepare('SELECT *  FROM tab_propriete as tpp  INNER JOIN propriete as prop ON tpp.`id_propriete`= prop.id_propriete  WHERE id_huile= :huile');
        $prop->bindParam(':huile',$id_huile);
        $prop->execute();

        $prob=$bdd->prepare('SELECT *  FROM tab_probleme as tpb  INNER JOIN probleme as prob ON tpb.`id_prob`= prob.id_probleme  WHERE id_huile= :huile');
        $prob->bindParam(':huile',$id_huile);
        $prob->execute();

        $list_prob=$bdd->query('SELECT * FROM probleme');

        
        ?>
        <!-- <div><button onclick="$('#contenu').load('./pages/list_edit_huile.php')" class="button"><--- Retour</button></div> -->

        <div class="cell large-7" style="padding: 2%; border:1px solid; margin:2%;">

            <form action="./data/update_huile.php" method="post" enctype="multipart/form-data">
                <input type="number" name="id" value="<?=$id_huile;?>" style="display: none;">
                <div>
                    <label for=""> Nom: </label>
                    <input type="text" name="nom" value="<?=$nom;?>">
                </div>
                <div>
                    <label for="">Description:</label> 
                    <textarea name="desc" id="" cols="30" rows="10"><?=$desc;?></textarea> 
                </div>
                <?php
                    if (isset($image)) 
                    {
                        ?>
                            <div>
                                <div>
                                    <input type='file' name="img" />
                                    <label for="" ></label>
                                </div>
                                <div>
                                    <div style="background-image: url('./ressources/images/huiles/<?=$image;?>');" >
                                    </div>
                                </div>
                            </div>
                        <?php
                    }
                    else
                    {   
                        ?>
                            <div>
                                <input type='file' name="img" />
                            </div>
                        <?php
                    }
                ?>
                
                <div>
                    <h3>Propriétés : </h3>
                    <?php 
                    $p=1;
                    while ($propriete=$prop->fetch()) 
                    {
                        ?>
                            <div style="padding: 2%;">
                                <label for="" >Propriété <?=$p;?> :</label>
                                <input type='text' name="propriete[]" value="<?=$propriete['nom'];?>"/>
                                <input type="number" name="propriete<?=$p;?>"  value="<?=$propriete['id_propriete'];?>" style="display: none;">
                                <p onclick="Delete_prop(<?=$propriete['id_propriete'];?>)" class="button">Supprimer propriété</p>
                            </div>
                        <?php
                        $p++;
                    }
                    ?>
                </div>
                <div>
                    <h3>Problèmes Traités : </h3>
                    <div class="grid-container">
                        <div class="grid-x probleme">
                    <?php  
                      
                    while ($probleme=$prob->fetch()) 
                    {
                        ?>
                            <div class=" cell large-4 probleme__bloc">
                                <label for="" ><?=$probleme['nom'];?></label>
                                <input type="checkbox" name="probleme[]" id="" class="probleme__input" value="<?=$probleme['nom'];?>" checked>
                            </div>
                        <?php
 
                    }      
                    ?>
                        </div>
                    </div>
                </div>
                <button type="submit" class="button">Envoyer</button>
            </form> 
            <button onclick="Delete_huile(<?=$id;?>)">Supprimer l'huile</button>
        </div>
    <?php   
    }
}
?>
<script>
    function Delete_huile(id)
    {
        if (confirm("Etes-vous sur de vouloir supprimer cette huile ?"))
        {
            window.location.replace("./data/delete_huile.php?id="+id+"")
        }
    }
    function Delete_prop(id)
    {
        if (confirm("Etes-vous sur de vouloir supprimer cette propriété ?"))
        {
            window.location.replace("./data/delete_propriete_huile.php?id_prop="+id+"")
        }
    }
</script>