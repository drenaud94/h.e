<?php
require_once './templates/header.php';


if (!isset($_SESSION['statut']) OR $_SESSION['statut'] !== '1') 
{
    header("Location: index.php");
    exit;
}

?>
<section>
    <div class="titre_admin">
        <h1>Gestion des Huiles Essentielles</h1>
    </div>
    <div class="grid-container">
        <div class="grid-x align-center">
            <div class="cell large-3 bouton_admin">
                <button class="button btn1" onclick="$('#contenu').load('./pages/modifier_huile.php')">Modifier</button>
            </div>
            <div class="cell large-3 bouton_admin">
                <button class="button btn2" onclick="$('#contenu').load('./pages/ajouter_huile.php')">Ajouter</button>
            </div>
        </div>
    </div>
    <div class="grid-container">
        <div class="grid-x align-center" id="contenu">

        </div>
    </div>
</section>
<script src="./ressources/js/bouton_actif.js"></script>