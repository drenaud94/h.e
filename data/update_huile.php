<?php
session_start();
include '../config/configuration.php';

if (isset($_POST['nom']) && isset($_POST['desc']) && isset($_POST['probleme']) && isset($_POST['propriete']) ) 
{
    $id_huile=$_POST['id'];
    $nom =$_POST['nom'];
    $desc = $_POST['desc'];

    $huile=$bdd->prepare('UPDATE huiles SET nom=:nom, description=:desc WHERE id_huile=:id_huile');
    $huile->bindParam(':nom',$nom);
    $huile->bindParam(':desc', $desc);
    $huile->bindParam(':id_huile', $id_huile);
    $huile->execute();

    //Ajout des propriétés et des liaisons avec les propriétés
    $p=1;
    foreach ($_POST['propriete'] as $value) 
    {
        if (!empty($value)) 
        {
            $prop=$bdd->prepare('UPDATE propriete as prop INNER JOIN tab_propriete as tpp ON prop.`id_propriete`= tpp.id_propriete SET  prop.nom =:nom WHERE id_huile= :huile AND prop.id_propriete=:id_prop');
            $prop->bindParam(':nom',$value);
            $prop->bindParam(':huile',$id_huile);
            $prop->bindParam(':id_prop',$_POST['propriete'.$p.'']);
            $prop->execute();

            $p++;
        }
    }
    
    //Ajout des liaisons avec les problèmes traités
    $p=1;
    foreach ($_POST['probleme'] as $value) 
    {
        if (!empty($value)) 
        {
            $prop=$bdd->prepare('UPDATE probleme as prop INNER JOIN tab_propriete as tpp ON prop.`id_propriete`= tpp.id_propriete SET  prop.nom =:nom WHERE id_huile= :huile AND prop.id_propriete=:id_prop');
            $prop->bindParam(':nom',$value);
            $prop->bindParam(':huile',$id_huile);
            $prop->bindParam(':id_prop',$_POST['propriete'.$p.'']);
            $prop->execute();

            $p++;
        }
    }

    //Ajout de l'image 
    if (isset($_FILES['img'])) 
    {
        $image=basename($_FILES['img']['name']);;
        $dossier = '../ressources/images/huiles/';
        $extensions = array('.png', '.gif', '.jpg', '.jpeg');
        $extension = strrchr($_FILES['img']['name'], '.');

        //vérifications extensions
        if(!in_array($extension, $extensions))
        //Si l'extension n'est pas dans le tableau
        {
            $erreur = 'Vous devez uploader un fichier de type png, gif, jpg ou jpeg...';
            header('Refresh:3; url=../Gestion_huile.php');
        }

        //S'il n'y a pas d'erreur on upload
        if(!isset($erreur))
        {
            //On formate le nom du fichier ici...
            $fichier = strtr($image,
                'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
            $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

            if(move_uploaded_file($_FILES['img']['tmp_name'], $dossier . $fichier))
            //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
            {

                $req = $bdd->prepare('UPDATE huiles SET image=:img WHERE id_huile=:huile'); 
                $req->bindParam(':img',$fichier);
                $req->bindParam(':huile',$id_huile);
                $req->execute();

            }
            else
            {
                
                echo 'Echec de l\'upload !';
                header('Refresh:3; url=../Gestion_huile.php');
            }
        }
    }       
   header('Location:../Gestion_huile.php');
}
else
{
    echo 'Une erreur est survenue!';
    header('Refresh:3; url=../Gestion_huile.php');
}