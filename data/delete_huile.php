<?php
include '../config/configuration.php';
$id_huile=intval($_GET['id']);

//Récupération des id des propriétés rattachées à l'huile
$recup_id_prop=$bdd->prepare('SELECT id_propriete FROM tab_propriete WHERE id_huile=:id_huile');
$recup_id_prop->bindParam(':id_huile', $id_huile);
$recup_id_prop->execute();

//Suppresion du lien entre huile et propriete + suppression de la propriété
while ($data = $recup_id_prop->fetch()) 
{
    $id_propriete=$data['id_propriete'];

    $delete_tab_prop=$bdd->prepare('DELETE FROM tab_propriete WHERE id_propriete=:id_propriete');
    $delete_tab_prop->bindParam(':id_propriete',$id_propriete);
    $delete_tab_prop->execute();

    $delete_prop=$bdd->prepare('DELETE FROM propriete WHERE id_propriete=:id_propriete');
    $delete_prop->bindParam(':id_propriete',$id_propriete);
    $delete_prop->execute();
}


//Récupération des id des problèmes rattachées à l'huile
$recup_id_prob=$bdd->prepare('SELECT id_prob FROM tab_probleme WHERE id_huile=:id_huile');
$recup_id_prob->bindParam(':id_huile', $id_huile);
$recup_id_prob->execute();

//Suppresion du lien entre huile et probleme
while ($data = $recup_id_prob->fetch()) 
{
    $id_probleme=$data['id_prob'];

    $delete_tab_prob=$bdd->prepare('DELETE FROM tab_probleme WHERE id_prob=:id_probleme');
    $delete_tab_prob->bindParam(':id_probleme',$id_probleme);
    $delete_tab_prob->execute();
}

//Suppresion des modes d'utilisation
$delete_mode=$bdd->prepare('DELETE FROM mode_utilisation WHERE id_huile=:id_huile');
$delete_mode->bindParam(':id_huile',$id_huile);
$delete_mode->execute();

//Suppresion de l'huile
$delete_huile=$bdd->prepare('DELETE FROM huiles WHERE id_huile=:id_huile');
$delete_huile->bindParam(':id_huile',$id_huile);
$delete_huile->execute();

echo('L\huile et toutes ses propriétés ont été supprimé !');
header('Refresh:2; url=../Gestion_huile.php');