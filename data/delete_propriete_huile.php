<?php
include '../config/configuration.php';
$id_prop=intval($_GET['id_prop']);

$delete_tab_prop=$bdd->prepare('DELETE FROM tab_propriete WHERE id_propriete=:id_propriete');
$delete_tab_prop->bindParam(':id_propriete',$id_prop);
$delete_tab_prop->execute();

$delete_prop=$bdd->prepare('DELETE FROM propriete WHERE id_propriete=:id_propriete');
$delete_prop->bindParam(':id_propriete',$id_propriete);
$delete_prop->execute();

echo('La propriété a été supprimé !');
header('Refresh:2; url=../Gestion_huile.php');