<?php
include '../config/configuration.php';

if (isset($_GET['id_pb'])) 
{
    $list_huile=$bdd->prepare('SELECT huiles.id_huile as id_huile, nom, description, image FROM huiles INNER JOIN tab_probleme as tab ON huiles.id_huile=tab.id_huile WHERE id_prob=:id_prob');
    $list_huile->bindParam(':id_prob',$_GET['id_pb']);
    $list_huile->execute();

    $list=[];
    while ($huile=$list_huile->fetch()) 
    {
        array_push($list,['id'=>$huile['id_huile'],'nom'=>$huile['nom'], 'desc'=>$huile['description'], 'img'=>$huile['image'] ]);
    }
    $json =json_encode($list);
    echo $json;
}