<?php
if (isset($_GET['type'])) 
{
    //données utilisées pour la page d'édition de contenu

    include('../config/configuration.php');
    $type=$_GET['type'];

    $data=$bdd->prepare('SELECT * FROM contenu WHERE id_type=:type');
    $data->bindParam(':type', $type);
    $data->execute();
    $info=[];
    if ($type==1) 
    {
        while($donnee=$data->fetch())
        {
            $id=$donnee['id_contenu'];
            $nom=$donnee['nom'];
            $desc=$donnee['description'];
            array_push($info,['id'=>$id,'nom'=>$nom,'desc'=>$desc]);
        }
        $json=json_encode($info);
    
        echo $json;
    }
    elseif ($type==2) 
    {
            while($donnee=$data->fetch())
        {
            $id=$donnee['id_contenu'];
            $nom=$donnee['nom'];
            $url=$donnee['lien'];
            array_push($info,['id'=>$id,'nom'=>$nom,'url'=>$url]);
        }
        $json=json_encode($info);

        echo $json;
    }
    
}
// else
// {
//     //données utilisées pour la page prestation
//     include('./config/configuration.php');
//     $data=$bdd->query('SELECT * FROM prestation');
//     //Du aux différents type de prestation on sépare dans 3 arrays pour ensuite les inclure plus facilement dans leur div respectives
//     $barbe=[];
//     $cheveux=[];
//     $soins=[];
//     while($donnee=$data->fetch())
//     {
//         $id=$donnee['id'];
//         $nom=$donnee['nom'];
//         $desc=$donnee['description'];
//         $prix=$donnee['prix'];
//         switch ($donnee['id_type']) 
//         {
//             case '1':
//                 array_push($barbe,['type'=>$donnee['id_type'],'nom'=>$nom,'desc'=>$desc,'prix'=>$prix]);
//                 break;
//             case '2':
//                 array_push($cheveux,['type'=>$donnee['id_type'],'nom'=>$nom,'desc'=>$desc,'prix'=>$prix]);
//                 break;
//             case '3':
//                 array_push($soins,['type'=>$donnee['id_type'],'nom'=>$nom,'desc'=>$desc,'prix'=>$prix]);
//                 break;
//             default:
//                 break;
//         } 
//     }
// }
