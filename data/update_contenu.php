<?php
session_start();
include '../config/configuration.php';

if (isset($_POST['nom']) AND (isset($_POST['desc']) || isset($_POST['url'])) AND isset($_POST['id'])) 
{
    $nom=$_POST['nom'];
    $id_contenu=$_POST['id'];

    if (isset($_POST['desc'])) 
    {
        $desc=$_POST['desc'];

        $edit_exercice=$bdd->prepare('UPDATE contenu SET nom=:nom , description=:desc WHERE id_contenu=:id_contenu');
        $edit_exercice->bindParam(':nom',$nom);
        $edit_exercice->bindParam(':desc',$desc);
        $edit_exercice->bindParam(':id_contenu',$id_contenu);
        $edit_exercice->execute();

        echo 'Les données ont été modifiées !';
        header('Refresh:2; url=../Gestion_meditation.php ');

    }
    elseif (isset($_POST['url'])) 
    {
        $url=$_POST['url'];
        $edit_video=$bdd->prepare('UPDATE contenu SET nom=:nom , lien=:lien  WHERE id_contenu=:id_contenu');
        $edit_video->bindParam(':nom',$nom);
        $edit_video->bindParam(':lien',$url);
        $edit_video->bindParam(':id_contenu',$id_contenu);
        $edit_video->execute();

        echo 'Les données ont été modifiées !';
        header("Refresh:2; url=../Gestion_meditation.php ");
    }
    else {
        echo 'Il manque des informations';
    }
}