<?php
session_start();
include '../config/configuration.php';

if (isset($_POST['nom']) AND (isset($_POST['desc']) || isset($_POST['url'])) AND isset($_POST['id'])) 
{
    $nom=$_POST['nom'];
    $id_type=$_POST['id'];

    if (isset($_POST['desc'])) 
    {
        $desc=$_POST['desc'];
        $exercice=$bdd->prepare('INSERT INTO contenu (nom,description, id_user, id_type ) VALUES (:nom, :desc, :user, :id_type)');
        $exercice->bindParam(':nom',$nom);
        $exercice->bindParam(':desc',$desc);
        $exercice->bindParam(':user',$_SESSION['id_user']);
        $exercice->bindParam('id_type',$id_type);
        $exercice->execute();

        echo 'Les données ont été enregistrées !';
        header('Refresh:2; url=../Gestion_meditation.php ');

    }
    elseif (isset($_POST['url'])) 
    {
        $url=$_POST['url'];
        $video=$bdd->prepare('INSERT INTO contenu (nom,lien, id_user, id_type ) VALUES (:nom, :lien, :user, :id_type)');
        $video->bindParam(':nom',$nom);
        $video->bindParam(':lien',$url);
        $video->bindParam(':user',$_SESSION['id_user']);
        $video->bindParam('id_type',$id_type);
        $video->execute();

        echo 'Les données ont été enregistrées !';
        header("Refresh:2; url=../Gestion_meditation.php ");
    }
    else {
        echo 'Il manque des informations';
    }
}