<?php
session_start();
include '../config/configuration.php';

if (isset($_POST['nom'])  && isset($_POST['desc']) && isset($_POST['probleme']) && isset($_POST['propriete']) && isset($_POST['mode_diffusion']) && isset($_POST['mode_alimentaire']) && isset($_POST['mode_massage']) && isset($_POST['mode_hygiene'])) 
{
    if (!empty($_POST['nom']) && !empty($_POST['desc'])) 
    {
        $nom =$_POST['nom'];
        $desc = $_POST['desc'];
        $diffusion = $_POST['mode_diffusion'];
        $alimentaire = $_POST['mode_alimentaire'];
        $massage = $_POST['mode_massage'];
        $hygiene = $_POST['mode_hygiene'];

        //Vérifier que l'huile n'existe pas déjà
        $verif = $bdd->prepare('SELECT nom FROM huiles WHERE nom= :nom');
        $verif->bindParam(':nom',$nom);
        $verif->execute();
        $exist=$verif->rowCount();

        //si l'huile n'existe pas
            if ($exist != '1') 
            {
                $huile=$bdd->prepare('INSERT INTO huiles (nom, description, id_user) VALUES (:nom, :desc, :user)');
                $huile->bindParam(':nom',$nom);
                $huile->bindParam(':desc', $desc);
                $huile->bindParam(':user', $_SESSION['id_user']);
                $huile->execute();

                //On récupére l'id de l'huile enregistrée pour faire les liaison
                $id_huile=$bdd->prepare('SELECT * FROM huiles WHERE nom = :huile');
                $id_huile->bindParam(':huile',$nom);
                $id_huile->execute();
                $id_huile=$id_huile->fetch();
                $id_huile=$id_huile['id_huile'];

                //Ajout des propriétés et des liaisons avec les propriétés
                foreach ($_POST['propriete'] as $value) 
                {
                    if (!empty($value)) 
                    {
                        $propriete =$bdd->prepare('INSERT INTO propriete (nom) VALUES (:nom)');
                        $propriete->bindParam(':nom',$value);
                        $propriete->execute();

                        //On récupére l'id de la propriété enregistrée pour faire les liaisons entre huile et propriété dans la table tab_propriete
                        $id_prop=$bdd->prepare('SELECT id_propriete FROM propriete WHERE nom = :value');
                        $id_prop->bindParam(':value',$value);
                        $id_prop->execute();
                        $id_prop=$id_prop->fetch();
                        $id_prop=$id_prop['id_propriete'];

                        $tab_prop = $bdd->prepare('INSERT INTO tab_propriete (id_propriete, id_huile) VALUES (:prop, :huile)');
                        $tab_prop->bindParam(':prop', $id_prop);
                        $tab_prop->bindParam(':huile', $id_huile);
                        $tab_prop->execute();
                    }
                }
                
                //Ajout des liaisons avec les problèmes traités
                foreach ($_POST['probleme'] as $values) 
                {
                    //On récupére l'id du probleme selectionné pour faire les liaisons entre huile et probleme dans la table tab_probleme
                    $id_prob=$bdd->prepare('SELECT id_probleme FROM probleme WHERE nom = :values');
                    $id_prob->bindParam(':values',$values);
                    $id_prob->execute();
                    $id_prob=$id_prob->fetch();
                    $id_prob=$id_prob['id_probleme'];

                    $tab_prob = $bdd->prepare('INSERT INTO tab_probleme (id_prob, id_huile) VALUES (:prob, :huile)');
                    $tab_prob->bindParam(':prob', $id_prob);
                    $tab_prob->bindParam(':huile', $id_huile);
                    $tab_prob->execute();
                }

                $mode=$bdd->prepare('INSERT INTO mode_utilisation (diffusion, alimentaire, massage, hygiene, id_huile) VALUES (:dif, :alim, :mas, :hyg, :huile)');   
                $mode->bindParam(':dif', $diffusion);
                $mode->bindParam(':alim', $alimentaire);
                $mode->bindParam(':mas', $massage);
                $mode->bindParam(':hyg', $hygiene);
                $mode->bindParam(':huile', $id_huile);
                $mode->execute();

                if (isset($_POST['new_probleme'])) 
                {
                    $new_pb=$_POST['new_probleme'];
                    foreach ($new_pb as $value) 
                    {
                        $new_probleme=$bdd->prepare('INSERT INTO probleme (nom) VALUES (:nom)');
                        $new_probleme->bindParam(':nom',$value);
                        $new_probleme->execute();

                        //On récupére l'id du nouveau probleme selectionné pour faire les liaisons entre huile et probleme dans la table tab_probleme
                        $id_new_prob=$bdd->prepare('SELECT id_probleme FROM probleme WHERE nom = :values');
                        $id_new_prob->bindParam(':values',$value);
                        $id_new_prob->execute();
                        $id_new_prob=$id_new_prob->fetch();
                        $id_new_prob=$id_new_prob['id_probleme'];

                        $tab_prob = $bdd->prepare('INSERT INTO tab_probleme (id_prob, id_huile) VALUES (:prob, :huile)');
                        $tab_prob->bindParam(':prob', $id_new_prob);
                        $tab_prob->bindParam(':huile', $id_huile);
                        $tab_prob->execute();
                    }
                    
                }
                
                //Ajout de l'image 
                if (isset($_FILES['img'])) 
                {
                    
                    $image=basename($_FILES['img']['name']);;
                    $dossier = '../ressources/images/huiles/';
                    $extensions = array('.png', '.gif', '.jpg', '.jpeg');
                    $extension = strrchr($_FILES['img']['name'], '.');

                    //vérifications extensions
                    if(!in_array($extension, $extensions))
                    //Si l'extension n'est pas dans le tableau
                    {
                        $erreur = 'Vous devez uploader un fichier de type png, gif, jpg ou jpeg...';
                        header('Refresh:3; url=../Gestion_huile.php');
                    }

                    //S'il n'y a pas d'erreur on upload
                    if(!isset($erreur))
                    {
                        //On formate le nom du fichier ici...
                        $fichier = strtr($image,
                            'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                            'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                        $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

                        if(move_uploaded_file($_FILES['img']['tmp_name'], $dossier . $fichier))
                        //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
                        {

                            $req = $bdd->prepare('UPDATE huiles SET image=:img WHERE id_huile=:huile'); 
                            $req->bindParam(':img',$fichier);
                            $req->bindParam(':huile',$id_huile);
                            $req->execute();

                        }
                        else
                        {
                            
                            echo 'Echec de l\'upload !';
                            header('Refresh:3; url=../Gestion_huile.php');
                        }
                    }
                }      
            }
    }
   
   header('Location:../Gestion_huile.php');
}
else
{
    echo 'Une erreur est survenue!';
    header('Refresh:3; url=../Gestion_huile.php');
}