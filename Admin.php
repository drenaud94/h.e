<?php
require_once './templates/header.php';

if (!isset($_SESSION['statut']) OR $_SESSION['statut'] !== '1') 
{
    header("Location: index.php");
    exit;
}

?>

<div class="grid-container">
    <div class="grid-x align-center">
        <div class="cell large-3 bouton_admin">
            <a href="./Gestion_huile.php"><button class="button">Gestion des Huiles Essentielles</button></a> 
        </div>
        
        <div class="cell large-3 bouton_admin">
            <a href="./Gestion_meditation.php"><button class="button">Gestion des Exercices de Méditations</button></a>
        </div>
    </div>
</div>