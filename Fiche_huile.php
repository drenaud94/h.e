<?php
include './config/configuration.php';
require './templates/header.php';

if (isset($_GET['id'])) 
{
    $id=$_GET['id'];

    $data=$bdd->prepare('SELECT * FROM huiles WHERE id_huile=:id');
    $data->bindParam(':id',$id);
    $data->execute();

    while ($donnees = $data->fetch()) 
    {
        $id_huile=intval($donnees['id_huile']);
        $nom=$donnees['nom'];
        $desc=$donnees['description'];
        $image=$donnees['image'];

        $prop=$bdd->prepare('SELECT *  FROM tab_propriete as tpp  INNER JOIN propriete as prop ON tpp.`id_propriete`= prop.id_propriete  WHERE id_huile= :huile');
        $prop->bindParam(':huile',$id_huile);
        $prop->execute();

        $prob=$bdd->prepare('SELECT *  FROM tab_probleme as tpb  INNER JOIN probleme as prob ON tpb.`id_prob`= prob.id_probleme  WHERE id_huile= :huile');
        $prob->bindParam(':huile',$id_huile);
        $prob->execute();

        $mode=$bdd->prepare('SELECT *  FROM mode_utilisation WHERE id_huile= :huile');
        $mode->bindParam(':huile',$id_huile);
        $mode->execute();

        ?>
<div class="grid-container">
    <div class="grid-x align-center">
        <div class="cell large-7" style="padding: 2%; border:1px solid; margin:2%;">

            <div>
                <h1>Huile Essentielle <?=$nom;?></h1>
            </div>
            <div>
                <p><?=$desc;?></p>
            </div>
            <div>
                <img src="./ressources/images/huiles/<?=$image;?>" alt="">
            </div>
            
            <div>
                <h3>Propriétés : </h3>
                <div>
                <?php 
                    $p=1;
                    while ($propriete=$prop->fetch()) 
                    {
                        ?>
                            <div style="padding: 2%;">
                                <p><?=$p.' : '.$propriete['nom'];?></p>
                            </div>
                        <?php
                        $p++;
                    }
                    ?>
                </div>
            </div>
            <div>
                <h3>Problèmes Traités : </h3>
                <div class="grid-container">
                    <div class="grid-x probleme">
                        <?php  
                        
                        while ($probleme=$prob->fetch()) 
                        {
                            ?>
                                <div class=" cell large-4 probleme__bloc">
                                    <div class="button"><?=$probleme['nom'];?></div>
                                </div>
                            <?php

                        }      
                        ?>
                    </div>
                </div>
            </div>
            <div>
                <h3>Mode d'utilisation</h3>
                <?php

                while($modes=$mode->fetch())
                {
                    ?>
                        <div>
                            <p>Diffusion : <?=$modes['diffusion'];?></p>
                        </div> 
                        <div>
                            <p>Alimentaire : <?=$modes['alimentaire'];?></p>
                        </div>
                        <div>
                            <p>Massage : <?=$modes['massage'];?></p>
                        </div>
                        <div>
                            <p>Hygiene : <?=$modes['hygiene'];?></p>
                        </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="cell">
                        <p>AVERTISSEMENT : Ces propriétés, indications et modes d'utilisation sont tirés des ouvrages ou sites Internet de référence en aromathérapie, hydrolathérapie et phytothérapie. On les y retrouve de façon régulière et pour beaucoup confirmés par des observations en milieu scientifique. Toutefois, ces informations sont données à titre informatif, elles ne sauraient en aucun cas constituer une information médicale, ni engager notre responsabilité. Pour tout usage dans un but thérapeutique, consultez un médecin</p>
        </div>
    </div>
</div>
        
<?php   
}
}
?>