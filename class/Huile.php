<?php

class Huile
{
    private $nom;
    private $description;
    private $image;
    private $id;
    private $id_user;

    public function __construct($id, $nom, $desc, $image, $id_user)
    {
        $this->setId($id);
        $this->setNom($nom);
        $this->setDesc($desc);
        $this->setImage($image);
        $this->setId_user($id_user);
    }
    public function setId($id)
    {
        $this->id=$id;
    }
    public function setNom($nom)
    {
        $this->nom=$nom;
    }
    public function setDesc($desc)
    {
        $this->description=$desc;
    }
    public function setImage($image)
    {
        $this->image=$image;
    }
    public function setId_user($id_user)
    {
        $this->id_user=$id_user;
    }

    public function getId()
    {
        return self::$id;
    }
    public function getNom()
    {
        return $this->nom;
    }
    public function getDesc()
    {
        return $this->description;
    }
    public function getImage()
    {
        return $this->image;
    }
    public function getId_user()
    {
        return self::$id_user;
    }
}
