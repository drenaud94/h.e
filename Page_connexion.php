<?php 
require_once './templates/header.php';

include './config/configuration.php';

if (isset($_POST['connexion']))
    {
        $login= htmlspecialchars($_POST['login']);
        $mp= md5(($_POST['password']));
            if (!empty($login) AND !empty($mp))
                {

                $req=$bdd->prepare("SELECT * FROM users WHERE login = :login AND password = :password ");
                $req->bindParam(':login', $login);
                $req->bindParam(':password', $mp);
                $req->execute();
                $exist=$req->rowCount();

                if ($exist == 1)
                {
                    $donnees=$req->fetch();

                    $_SESSION['login'] = $donnees['login'];
                    $_SESSION['statut'] = $donnees['id_droit'];
                    $_SESSION['id_user'] = $donnees['id_user'];
                    // $_SESSION['password'] = $donnees['password'];
                    header("Location: index.php");
                }
                else
                { 
                    echo('Mauvais login ou mauvais mot de passe!');
                }
            }
            else
            {
                echo('Tous les champs doivent être remplis');
            }
    }   	
?>
<div class="grid-container connexion ">
    <div class="grid-x align-center">
        <div class="cell large-6 connexion_bloc">
            <div class="grid-container">
                <div class="grid-x align-center">
                    <div class="cell connexion_titre">
                        <h2>Login</h2>
                    </div>
                    <div class="cell connexion_form">
                        <form action="" method="POST">
                            <div class="grid-container">
                                <div class="grid-x grid-padding-x align-center">
                                    <div class="cell medium-6 large-8">
                                        <label>Nom d'utilisateur
                                        <input type="text" name="login" placeholder="user..." class="champ">
                                        </label>
                                    </div>
                                    <div class="cell medium-6 large-8">
                                        <label>Mot de passe
                                        <input type="text" name="password" placeholder="password..." class="champ">
                                        </label>
                                    </div>
                                    <div class="cell medium-3 large-8">
                                        <div>
                                            <input type="checkbox" name="" id=""> : Se rappeler de moi 
                                        </div>
                                        <button class="button" name="connexion" type="submit">Login</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>