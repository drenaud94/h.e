<?php
require_once './templates/header.php';
include './config/configuration.php';

$pb=$bdd->query('SELECT * FROM probleme');
$huile=$bdd->query('SELECT * FROM huiles');

?>
<div class="grid-container fluid banniere">
    <div class="grid-x align-center">
        <div class="cell small-12 medium-12 large-12 parallax banniere_huile">
            <div class="grid-container fluid">
                <div class="grid-x align-center">
                    <div class="cell small-12 medium-10 large-4 banniere_title">
                        <h1>Huiles Essentielles</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="huile">
    <div class="grid-container fluid">
        <div class="grid-x align-center">
            <div class="cell small-12 medium-9 large-7">
                <div class="grid-container fluid">
                    <div class="grid-x ">
                        <div class="cell huile_title">
                            <h1>Guide des huiles essentielles</h1>
                        </div>
                    </div>
                </div>
                <div class="grid-container fluid">
                    <div class="grid-x grid-margin-x">
                        <div class="cell small-8 medium-5 large-4">
                            <img src="./ressources/images/header_zen.jpg" alt="">
                        </div>
                        <div class="cell small-12 medium-7 large-7 small-offset-0.5 desc">
                            <p>Utile et pratique, ce guide d’utilisation vous permettra de découvrir les vertus de
                                chaque huile essentielle et de trouver rapidement l’huile essentielle la mieux adaptée à
                                vos besoins. En diffusion, sur la peau, en usage interne, dans le bain, en massage ou en
                                inhalation : apprenez à les utiliser au mieux et en toute sécurité grâce à l’expertise
                                et au savoir-faire de l’équipe de scientifiques Aroma-Zone. Santé, beauté, bien-être…
                                quelles huiles essentielles pour quel usage ? Et quelles huiles essentielles pour quels
                                maux ? Découvrez les fabuleux bienfaits de l’aromathérapie au quotidien et trouvez la
                                meilleure huile essentielle pour répondre à votre problématique !</p>
                        </div>
                    </div>
                </div>
                <div class="grid-container fluid filter">
                    <div class="grid-x align-center" id="filtre">
                        <div class="cell small-12 medium-12 large-8 contenu">
                            <h3>Trouver une huile essentielle pour : </h3>
                            <form action="" method="get">
                                <select name="" id="huile_select">
                                    <option value=""></option>
                                <?php                             
                                    while ($data=$pb->fetch()) 
                                    {
                                        ?>
                                        <option value="<?=$data['id_probleme'];?>" ><?=$data['nom'];?></option>
                                        <?php
                                    }
                                ?>
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
                
                <div class="grid-container fluid">
                    <div class="grid-x grid-margin-x align-center" id="contenu_huile">

                    <?php
                    while($data=$huile->fetch())
                    {
                        ?>
                            <div class="cell small-4 medium-3 large-3 product-card" onclick="Fiche(<?=$data['id_huile'];?>)">
                                <div class="product_img">
                                    <img src="./ressources/images/huiles/<?=$data['image'];?>" alt="">
                                </div>
                                <div class="product_desc">
                                    <h3>Huile essentielle <?=$data['nom'];?></h3>
                                </div>
                            </div>
                        <?php
                    }
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function Fiche(id)
    {
        window.location.replace('./Fiche_huile.php?id='+id+'')
    }

    $('#huile_select').change(function()
    {
        $('#contenu_huile').text('')
        var id_pb = $('#huile_select option:selected').val()
        console.log(id_pb)
        var test = $.isNumeric(id_pb)
        if (test ===true) 
        {
            $.ajax({
                url:'./data/huile_par_pb.php?id_pb='+id_pb,
                dataType:'json',
                success: function(data){
                    
                    var longueur = data.length
                    var l=0;
                    //boucle pour ajouter les résultats obtenus
                    while (l<longueur) 
                        {
                            $('#contenu_huile').append('<div class="cell small-4 medium-3 large-3 product-card"> <div class="product_img"><img src="./ressources/images/huiles/'+data[l]['img']+'" alt=""></div> <div class="product_desc"><h3>Huile essentielle '+data[l]['nom']+'</h3></div></div>')
                            var l= l+1
                        }
                                        
                },              
            });   
        }
        else
        {
            location.reload()
        }
    })
    
</script>