<?php
include './templates/header.php';
?>
<div class="grid-container fluid banniere">
    <div class="grid-x align-center">
        <div class="cell small-12 medium-12 large-12 parallax banniere_huile">
            <div class="grid-container fluid">
                <div class="grid-x align-center">
                    <div class="cell small-12 medium-10 large-7 banniere_title">
                        <h1>Exercices de Méditation</h1>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>

<div class="grid-container fluid">
    <div class="grid-x align-center ">
        <div class="cell small-5 medium-4 large-2" >
            <div class="btn_exo" id="exos">
                <div class="ico ico_1" ></div>
            </div>
            <div class="titre_exo">Exercices de Respiration</div>
        </div>
        <div class="cell small-5 medium-4 large-2 large-offset-2">
            <div class="btn_exo" id="guide">
                <div class="ico ico_2"></div>
            </div>
            <div class="titre_exo">Méditations guidées</div>
        </div>
    </div>               
</div>

<div class="grid-container fluid meditation" id="explication">
    <!-- <div id="back"></div> -->
    <div class="grid-x align-center" id="content">
        <div class="cell medium-6 large-4 meditation_desc">
            <h2>La méditation c'est quoi ?</h2>
            <div>
                <p>La méditation est une pratique d’entraînement de l’esprit favorisant le bien-être mental</p>
                <p>Méditer c'est s'arrêter. Prendre du temps pour soi en se mettant à l'écart du monde un instant.</p>
            </div>       
        </div>
    </div>
</div>
<div class="grid-container fluid meditation" >
    <div class="grid-x align-center" id="contents">
        
    </div>
</div>

<script>
$('#contents').css('display','none');

$('#exos').click(function(){
    $('#contents').load('./pages/Meditation_respiration.php');

    if ($('#contents').css('display')=='none') 
    {
        $('#explication').css('display','none');
        $('#contents').css('display','flex');
        setTimeout(function(){$('html').animate({scrollTop: $(".meditation_exo").offset().top}, 1000)},1000);
    }
    else if ($('#contents').css('display')=='flex') 
    {
        if ($('.meditation_exo').css('display')=='block') {
            $('#contents').css('display','none');
            $('#explication').css('display','block');
        }      
    }
})

$('#guide').click(function(){
    $('#contents').load('./pages/Meditation_guide.php');

    if ($('#contents').css('display')=='none') 
    {
        $('#guide').addClass('btn_pousse')
        $('#explication').addClass('animate__animated').addClass('fadeOut')
        setTimeout(function(){})
        $('#explication').css('display','none');
        $('#contents').css('display','flex');
        setTimeout(function(){$('html').animate({scrollTop: $(".meditation_video").offset().top}, 1000)},1000);
    }
    else if ($('#contents').css('display')=='flex') 
    {
        if ($('.meditation_video').css('display')=='block') {
            
            $('#guide').removeClass('btn_pousse')
            $('#contents').css('display','none');
            $('#explication').css('display','block');

        }        
    }
})
</script>
  