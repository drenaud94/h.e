<?php
require_once './templates/header.php';
?>
<div class="grid-container fluid">
        <div class="grid-x align-center">
            <div class="cell small-12 medium-11 large-11 parallax header">
                <div class="grid-container fluid header_container">
                    <div class="grid-x align-center">
                        <div class="cell small-12 medium-10 large-5 citation">
                            <h3>"Ne demeure pas dans le passé, ne rêve pas du futur, concentre ton esprit sur le moment présent" <q>Bouddha</q></h3>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>

    <section class="section2">
        <div class="grid-container fluid">
            <div class="grid-x align-center">
                <div class="cell small-6 medium-6 large-4">
                    <div class="section2_titre">
                        <h1>Mon Projet</h1>
                    </div>
                    <div class="section2_info">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam, consectetur aperiam sint reiciendis voluptatibus sit harum non vero fugiat consequatur perferendis. Aliquam blanditiis quia cum natus facere sit dolorem cumque!</p>
                        <button><a href="">En savoir plus</a></button>
                    </div>
                        
                </div>
            </div>
        </div>        
    </section>
    
    <div class="grid-container fluid">
        <div class="grid-x align-center">
            <div class="cell small-12 medium-11 large-11 info">
                <div class="grid-container fluid">
                    <div class="grid-x">
                        <div class="cell small-12 medium-6 large-6 static">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam, consectetur aperiam sint reiciendis voluptatibus sit harum non vero fugiat consequatur perferendis. Aliquam blanditiis quia cum natus facere sit dolorem cumque!</p>
                        </div>
                        <div class="cell small-12 medium-6 large-6 bg2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> 