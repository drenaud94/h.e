<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css"
        integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2? family = Quicksand & display = swap" rel="feuille de style ">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="./ressources/css/style.css">

    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/js/foundation.min.js"
        integrity="sha256-pRF3zifJRA9jXGv++b06qwtSqX1byFQOLjqa2PTEb2o=" crossorigin="anonymous"></script>
    <script src="./ressources/js/controller.js"></script>

    <title>PWA</title>

</head>
<header>
    <div class="top-bar">
        <div class="top-bar-left">
            <div class="cell small-2 titre">
                <h1>ZENITUDE</h1>
            </div>
        </div>
        <div class="top-bar-right">
            <ul class="menu">
                <li><a href="index.php" class='lien'>Accueil</a></li>
                <li><a href="Essence.php" class='lien'>Huiles Essentielles</a></li>
                <li><a href="Meditation.php" class='lien'>Exercices de Méditation</a></li>
                <li><a href="" class='lien'>Contact</a></li>
                <?php
                    if (isset($_SESSION['statut']) AND $_SESSION['statut'] =='1') 
                    {
                        echo ('<li><a href="./Admin.php">Administrer</a></li>');
                    }
                    if (isset($_SESSION['login'])) 
                    {
                        echo ('<li><a href="./data/deconnexion.php">Deconnexion</a></li>');
                    }
                    else
                    {
                        echo ('<li><a href="./Page_connexion.php">Connexion</a></li>');
                    }
                ?>
            </ul>
        </div>
    </div>
    <!-- <div class="grid-x head align-middle">
        <div class="cell small-2 titre">
            <h1>ZENITUDE</h1>
        </div>
        <div class="cell small-10  ">
            <input type="checkbox" id="menu-checkbox" class="menu-checkbox">
            <label for="menu-checkbox" class="menu_toggle" id="menu_toggle">Show</label>
            <ul class="nav">
                <li><a href="index.php" class='lien'>Accueil</a></li>
                <li><a href="Essence.php" class='lien'>Huiles Essentielles</a></li>
                <li><a href="Meditation.php" class='lien'>Exercices de Méditation</a></li>
                <li><a href="" class='lien'>Contact</a></li>
               
            </ul>
        </div>
    </div> -->
</header>

<body>

</body>

</html>